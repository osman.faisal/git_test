print("Hello")
xmax = 10
ymax = 5
NEndUsers = 100
NMidSuppliers = 30
NEndSuppliers = 50
ms_catchment_dist = 4
es_catchment_dist = 3
import random

def CreateEndUser(xcoord, ycoord):
    return [xcoord, ycoord]

def CreateEndUserPop(nendusers):
    EndUserPop = []
    for i in range(nendusers):
        x = random.random() * xmax
        y = random.random() * ymax
        Enduser = CreateEndUser(x, y)
        EndUserPop.append(Enduser)
    return EndUserPop

# Create a population of businesses supplying end users

def CreateEndSupplier(xcoord, ycoord):
    return [xcoord, ycoord]

def CreateEndSupplierPop(nsuppliers):
    EndSupplierPop = []
    for i in range(nsuppliers):
        x = random.random() * xmax
        y = random.random() * ymax
        EndSupplier = CreateEndSupplier(x, y)
        EndSupplierPop.append(EndSupplier)
    return EndSupplierPop

def CreateMidSupplier(xcoord, ycoord):
    return [xcoord, ycoord]

def CreateMidSupplierPop(nsuppliers):
    MidSupplierPop = []
    for i in range(nsuppliers):
        x = random.random() * xmax
        y = random.random() * ymax
        MidSupplier = CreateMidSupplier(x, y)
        MidSupplierPop.append(MidSupplier)
    return MidSupplierPop



def distance(xy1, xy2):
    #    dist = ((coord1(0) - x1)^2 +(y2 - y1)^2)^0.5
    dist = (((xy1[0] - xy2[0]) ** 2) + (xy2[1] - xy2[1]) ** 2) ** 0.5
    return dist

# Calculate a metric for the value of the location of an End Supplier wrt end Users
# Simple metric.  Number of End Users within certain distance.

def EndSupplierValue(EndSupplier, catchment_dist):
    count = 0
    EndSupplierCoords = [EndSupplier[0], EndSupplier[1]]
    for i in range(NEndUsers):
        dist = distance(EndSupplierCoords, EndUserPop[i])
        if dist < catchment_dist:
            count = count + 1
    return count

# Calculate a metric for the value of the location of a Mid Supplier wrt end Users
# Simple metric.  Number of End Users within certain distance.

def MidSupplierValue(MidSupplier, radius):
    count = 0
    MidSupplierCoords = [MidSupplier[0], MidSupplier[1]]
    for i in range(NEndUsers):
        dist = distance(MidSupplier, EndUserPop[i])
        if dist < radius:
            count = count + 1
    return count

#Create populations

MidSupplierPop = CreateMidSupplierPop(NMidSuppliers)
EndSupplierPop = CreateEndSupplierPop(NEndSuppliers)
EndUserPop = CreateEndUserPop(NEndUsers)

# Calculate values of each mid- and end- supplier

for i in range(NMidSuppliers):
    value = MidSupplierValue(MidSupplierPop[i], ms_catchment_dist)
    MidSupplierPop[i].append(value)

for i in range(NEndSuppliers):
    value = EndSupplierValue(EndSupplierPop[i], es_catchment_dist)
    EndSupplierPop[i].append(value)